const express = require('express');
const routes = require('./routes/index');
//const usersController = require('./controller/users');
const bodyParser = require('body-parser');
const mongoose = require('./db/mongoose');

let app = express();
const port = 3000;

app.use(bodyParser.json());
app.use('/', routes);


app.listen({port}, () => {
    console.log(`Started up at port ${port}`);
})
