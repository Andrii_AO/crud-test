//const express = require('express');
const bodyParser = require('body-parser');
const {ObjectID} = require('mongodb');
const profileController = require('../controller/profile');

let User = require('./../model/user');
let Profile = require('./../model/profile');
let Image = require('../model/image');

function saveUser(req, res) {
//    console.log(req.body);
    let user = new User({
        email: req.body.email,
        password: req.body.password,
        profile: req.body.profile
    });

    user.save().then((doc) => {
        res.send(doc);
    }, (e) => {
        res.status(400).send(e);
    });
}

function readAllUsers(req, res) {
    User.find().then((users) => {
        res.send({users});
    }, (e) => {
        res.status(400).send(e);
    })
}

function readUser(req, res) {
    let id = req.params.id;

    if(!ObjectID.isValid(id)) {
        return res.status(404).send();
    }

    console.log(id);
    User.findById(id).then((user) => {
        if(!user) {
            console.log(user);
            return res.status(400).send();
        }
  
        res.send({user});
    }, (e) => {
        res.status(400).send(e);
    })

}

function removeUser(req, res) {
    let id = req.params.id;

    if(!ObjectID.isValid(id)) {
        return res.status(404).send();
    }

    User.findByIdAndRemove(id).populate('profile').then((user) => {
        if(!user) {
            return res.status(404).send();
        }

        Profile.findByIdAndRemove(user.profile).then((profile) => {
            profile.images.forEach((image) => {
                Image.findByIdAndRemove(image).then((image) => {
                        console.log(image);
                    });
                    res.send({user});
            }) 
        })
    }).catch((e) => {
        res.status(400).send();
    });
}

function updateUserById(req, res) {
    let id = req.params.id;

    if(!ObjectID.isValid(id)) {
        return res.status(404).send();
    }

    User.findByIdAndUpdate(id, {$set: req.body}).then((user) => {
        if(!user) {
            return res.status(404).send();
        }
        
        res.send({user});
    }).catch((e) => {
        res.status(400).send();
    });
}


module.exports = {
    saveUser,
    readAllUsers,
    readUser,
    removeUser,
    updateUserById
}