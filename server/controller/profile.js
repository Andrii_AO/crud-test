const {ObjectID} = require('mongodb');

let Profile = require('./../model/profile');

function saveProfile(req, res) {
    let profile = new Profile({
        firstName: req.body.firstName,
        lastName: req.body.lastName,
        phone: req.body.phone,
        address: req.body.address,
        images: req.body.images
    })

    profile.save().then((doc) => {
        res.send(doc);
    }, (e) => {
        res.status(400).send(e);
    });
};

function readAllProfiles(req, res) {
    Profile.find().then((profile) => {
        res.send({profile});
    }, (e) => {
        res.status(400).send(e);
    })
}

function readProfile(req, res) {
    let id = req.params.id;

    if(!ObjectID.isValid(id)) {
        return res.status(404).send();
    }

    Profile.findById(id)
//    .populate('images')
    .then((profile) => {
        if(!profile) {
            return res.status(400).send();
        }
  
        res.send({profile});
    }, (e) => {
        res.status(400).send(e);
    })

}

function removeProfile(req, res) {
    let id = req.params.id;

    if(!ObjectID.isValid(id)) {
        return res.status(404).send();
    }

    Profile.findById(id).then((profile) => {
        if(!profile) {
            return res.status(400).send();
        }

        profile.images.forEach((image) => {
            Image.findById(image.id).then((image) => {
                res.send({profile});               
            })
        })
    }).catch((e) => {
        res.status(400).send();
    });
}

function updateProfile(req, res) {
    const id = req.params.id;

    if(!ObjectID.isValid(id)) {
        return res.status(404).send();
    }

    Profile.findByIdAndUpdate(id, {$set: req.body}).then((profile) => {
        if(!profile) {
            return res.status(404).send();
        }

        res.send({profile});
    }).catch((e) => {
        res.status(400).send();
    })
}


module.exports = {
    saveProfile,
    readAllProfiles,
    readProfile,
    removeProfile,
    updateProfile
}
