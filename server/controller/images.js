const {ObjectID} = require('mongodb');

let Image = require('../model/image');

function saveImage(req, res) {
    let image = new Image({
        name: req.body.name,
        url: req.body.url,
        status: req.body.status
    })

    image.save().then((doc) => {
        res.send(doc);
    }, (e) => {
        res.status(400).send(e);
    });
}

function readAllImages(req, res) {
    Image.find().then((image) => {
        res.send({image});
    }, (e) => {
        res.status(400).send(e);
    })
}

function readImage(req, res) {
    let id = req.params.id;

    console.log(id);
    if(!ObjectID.isValid(id)) {
        return res.status(404).send();
    }
    Image.findById(id).then((image) => {
        if(!image) {
            return res.status(400).send();
        }
        console.log(image);

        res.send({image});
    }, (e) => {
        res.status(400).send(e);
    })

}

function removeImage(req, res) {
    let id = req.params.id;

    if(!ObjectID.isValid(id)) {
        return res.status(404).send();
    }

    Image.findByIdAndRemove(id).then((image) => {
        if(!image) {
            return res.status(404).send();
        }
        res.send({image});
    }).catch((e) => {
        res.status(400).send();
    })

}

function updateImage(req, res) {
    const id = req.params.id;

    if(!ObjectID.isValid(id)) {
        return res.status(404).send();
    }

    Image.findByIdAndUpdate(id, {$set: req.body}).then((image) => {
        if(!image) {
            return res.status(404).send();
        }

        res.send({image});
    }).catch((e) => {
        res.status(400).send();
    })
}

module.exports = {
    saveImage,
    readAllImages,
    readImage,
    removeImage,
    updateImage
}