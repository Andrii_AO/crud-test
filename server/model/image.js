let mongoose = require('mongoose');

let imageSchema = mongoose.Schema({
    name: {
        type: String
    },
    url: { 
        type: String
    },
    status: {
        type: String
    }
});

 
module.exports = mongoose.model('Image', imageSchema);