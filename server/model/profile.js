let mongoose = require('mongoose');

let Schema = mongoose.Schema;

let profileSchema = new Schema({
    firstName: {
        type: String
    },
    lastName: {
        type: String
    },
    phone: {
        type: String
    },
    address: {
        type: String
    }
    ,images: [{
        type: Schema.Types.ObjectId, 
        ref: 'Image'
    }]
});

module.exports = mongoose.model('Profile', profileSchema);
