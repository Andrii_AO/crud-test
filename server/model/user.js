let mongoose = require('mongoose');
let Schema = mongoose.Schema;

let userSchema = new Schema({
    email: {
        type: String
    }, 
    password: {
        type: String
    },
    profile: {
        type: Schema.Types.ObjectId,
        ref: 'Profile'
    }
});

module.exports = mongoose.model('User', userSchema);