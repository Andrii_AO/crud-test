const express = require('express');
const router = express.Router();
const usersController = require('./../controller/users');
const profileController = require('../controller/profile');
const imagesController = require('../controller/images');

// USERS ROUTES 
router.post('/users', usersController.saveUser);

router.get('/users', usersController.readAllUsers);

router.get('/users/:id', usersController.readUser);

router.delete('/users/:id', usersController.removeUser);

router.patch('/users/:id', usersController.updateUserById);

// PROFILE ROUTES
router.post('/profiles', profileController.saveProfile);

router.get('/profiles', profileController.readAllProfiles);

router.get('/profiles/:id', profileController.readProfile);

router.delete('/profiles/:id', profileController.removeProfile);

router.patch('/profiles/:id', profileController.updateProfile);

// IMAGE ROUTES
router.post('/images', imagesController.saveImage);

router.get('/images', imagesController.readAllImages);

router.get('/images/:id', imagesController.readImage);

router.delete('/images/:id', imagesController.removeImage);

router.patch('/images/:id', imagesController.updateImage);


module.exports = router;